package com.example.signin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }


    public fun signInMessage(view: View) {
        if (email.text.isEmpty() && password.text.isEmpty()) {
            view.isClickable = false
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
        else{
            checkEmail()
        }



    }

    private fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }
    private fun checkEmail() {
        if (isEmailValid(email.text.toString())) {
            Toast.makeText(applicationContext, "Authentication successful", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(applicationContext, "Invalid Email", Toast.LENGTH_SHORT).show()
        }
    }

    public fun signUpMessage(view: View) {
        view.isClickable = false
        Toast.makeText(this, "Sorry, not available yet", Toast.LENGTH_SHORT).show()


    }


}
